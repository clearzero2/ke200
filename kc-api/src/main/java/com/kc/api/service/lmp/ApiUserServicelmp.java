package com.kc.api.service.lmp;

import com.kc.api.mapper.UserMapper;
import com.kc.api.model.ClassTimeEntity;
import com.kc.api.model.User;
import com.kc.api.service.ApiUserService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.apache.commons.codec.digest.DigestUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * author shish
 * Create Time 2019/1/11 16:14
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Service
public class ApiUserServicelmp implements ApiUserService {
    @Autowired
    private UserMapper userMapper;
    @Override
    public User queryObject(@Param("userId") Long  userId) {
        User user=userMapper.queryObject(userId);
        return user;
    }

    @Override
    public long login(Long number, String password) {
            User user=userMapper.queryObject(number);
            Long userId=user.getUserId();
            if (DigestUtils.sha256Hex(password).equals(user.getPassword())){
                return userId;
            }else {
                return 0;
            }
    }

    @Override
    public List<ClassTimeEntity> QuerySignList(String grade, String time, String endtime) {
        return userMapper.QuerySignList(grade,time,endtime);
    }

    @Override
    public Integer save(User user) {
        user.setPassword(DigestUtils.sha256Hex(user.getPassword()));
        return userMapper.save(user);
    }

    @Override
    public User getUserByOpenId(String openid) {
        return userMapper.getUserByOpenId(openid);
    }

    @Override
    public List<ClassTimeEntity> getClassTimeEntities(User user) {
        String grade = user.getGrade();//年级
        Date date1 = new Date(System.currentTimeMillis());
        date1.setHours(0);
        date1.setMinutes(0);
        date1.setSeconds(0);
        Date date2 = new Date(System.currentTimeMillis());
        date2.setHours(23);
        date2.setMinutes(59);
        date2.setSeconds(59);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        sdf.format(date1);
        sdf.format(date2);
        System.out.println(sdf.format(date1));
        List<ClassTimeEntity> list = QuerySignList(grade, sdf.format(date1), sdf.format(date2));
        return list;
    }
}
