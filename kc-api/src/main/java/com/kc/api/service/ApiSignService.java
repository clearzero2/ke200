package com.kc.api.service;

import com.kc.api.model.SignEntity;

import java.util.List;

/**
 * author shish
 * Create Time 2019/5/3 12:52
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface ApiSignService {
    Integer signSave(SignEntity signEntity);

    SignEntity querySignList(Integer stu_id, Integer course_id, String queryDate, String queryDateEnd);

    List<SignEntity> querylistByUserId(Long userId);
     //不正常的课程
    List<SignEntity> querySignlistByUserId(Long userId);
}
