package com.kc.api.mapper;

import com.kc.api.model.TokenEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * author shish
 * Create Time 2019/1/11 18:02
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Mapper
public interface ApiTokenMapper {
    TokenEntity queryByTokenUserId(long userId);

    void update(TokenEntity tokenEntity);

    void save(TokenEntity tokenEntity);
    TokenEntity queryByToken(@Param("token") String token);
}
