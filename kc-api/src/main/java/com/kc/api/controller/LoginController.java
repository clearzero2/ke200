package com.kc.api.controller;

import com.kc.api.mapper.UserMapper;
import com.kc.api.annotation.IgnoreAuth;
import com.kc.api.annotation.LoginUser;
import com.kc.api.common.R;
import com.kc.api.model.User;
import com.kc.api.service.ApiUserService;
import com.kc.api.service.TokenService;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * author shish
 * Create Time 2019/1/11 15:25
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@RestController
@RequestMapping("/api")
public class LoginController {
    @Autowired
    private ApiUserService userService;
    @Autowired
    private TokenService tokenService;

    @IgnoreAuth
    @PostMapping("/login")
    @SneakyThrows
    public R login(@RequestBody LoginModel loginModel) {
        User user = userService.queryObject(loginModel.getNumber());
        if (user != null) {
            //生成token
            Map<String, Object> map = tokenService.createToken(user.getUserId());
            map.put("erro", 1);
            //用戶信息放入map中
            map.put("user", user);
            return R.ok(map);
        } else {
            throw new Exception("用户不存在");
        }
    }

}
