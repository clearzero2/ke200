package com.kc.api.controller;

import com.kc.api.annotation.IgnoreAuth;
import com.kc.api.annotation.LoginUser;
import com.kc.api.common.R;
import com.kc.api.model.TokenEntity;
import com.kc.api.model.User;
import com.kc.api.service.ApiUserService;
import com.kc.api.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * author shish
 * Create Time 2019/5/2 21:43
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@RestController
@RequestMapping("apiuser")
public class ApiGetLoginInfoController {
    @Autowired
    private TokenService tokenService;
    @Autowired
    private ApiUserService apiUserService;

    /**
     * 获取用户信息
     */
    @RequestMapping("/info")
    public R getUserInfo(@RequestBody @LoginUser User user) {
        Map<String, Object> result = new HashMap<>();
        User user1 = apiUserService.queryObject(user.getUserId());
        result.put("user", user1);
        return R.ok(result);
    }

    /**
     * 获取用户信息token
     */
    @RequestMapping("/token")
    @IgnoreAuth
    public R getToken(String token) {
        TokenEntity tokenEntity = tokenService.queryByToken(token);
        Map<String, Object> result = new HashMap<>();
        result.put("token", tokenEntity);
        return R.ok(result);
    }

    /**
     * 用户注册接口
     */
    @RequestMapping("/regist")
    @IgnoreAuth
    public R regist(User user) {
        Map<String, Object> result = new HashMap<>();
        Integer code = apiUserService.save(user);
        try {

            result.put("code", code);
        } catch (Exception e) {
            code = 1000;
            result.put("code", code);
            e.printStackTrace();
        }
        return R.ok(result);
    }

}
