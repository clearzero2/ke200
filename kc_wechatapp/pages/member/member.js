var request = require("../utils/request.js")
// var url=require('../config/config.js')
  var http  = require("../utils/request.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
      user:null
  },
  total:function(){
   wx.navigateTo({
     url: '../total/total',
   })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var data = wx.getStorageSync('user');
    console.log(data)
    var postData = {
      "userId":data.user.userId
    }
    var that = this
    http.post("/apiuser/info",postData).then((res)=>{
      console.log(res)
      that.setData({
        user: res.user
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // var that = this
    // // islogin.islogin()
    // request
    // that.getUserIfo().then(res => {
    //   console.log("异步结果齐威王乔沃维奇")
    //   console.log(res)
    //   that.setData({
    //     stu_id: res.data.user.userId,
    //     dept_id: res.data.user.class_grade,
    //     stu_name: res.data.user.name,
    //     grade: res.data.user.grade
    //   })
    // })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },


  loginoff: function () {
    wx.removeStorage({
      key: 'loginName',
      success: function (res) {
        console.log("退出成功")
        wx.showLoading({
          title: '正在退出',
          mask: true,
          success: function (res) {
            wx.removeStorage({
              key: 'token',
              success: function (res) {
                wx.showToast({
                  title: '清空数据',
                })
              },
            })
          },
          fail: function (res) { },
          complete: function (res) { },
        })
        setTimeout(function () {
          wx.reLaunch({
            url: '../login/index',
            success: function (res) { },
            fail: function (res) { },
            complete: function (res) { },
          });
          wx.hideLoading()
        }, 1500)

      },
    })

  },
  getUserIfo: function () {
    var that = this
    console.log(url["getUserInfo"])
    return new Promise((resolve, reject) => {
      wx.request({
        url: url["getUserInfo"],
        header: {
          'Content-Type': 'application/x-www-form-urlencoded',
          'X-Nideshop-Token': wx.getStorageSync('token'),
          'jhq-request-from': 'true'
        },
        success: function (res) {
          console.log(res)
          resolve(res)
        },
        fail: function (res) {
          reject(res)
        }
      })
    })

  }

})