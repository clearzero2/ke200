var amapFile = require('../libs/amap-wx.js');
var markersData = {
  latitude: '', //纬度
  longitude: '', //经度
  key: "1c6c7dba50dfdba107644ceaa09a16ae" //申请的高德地图key
};
Page({

  /**
   * 页面的初始数据
   */
  data: {
    latitude: '',
    longitude: '',
    localDesc: '',
    localName: '',
    markers: [{
      iconPath: "../imges/local.png",
      id: 0,
      width: 35,
      height: 45
    }],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this
    var myAmapFun = new amapFile.AMapWX({
      key: markersData.key
    })
    wx.showLoading({
      title: '童鞋，正在定位呢！！！！',
    })
    myAmapFun.getRegeo({
      success: (res) => {
        console.log(res, res[0].regeocodeData.formatted_address)
        this.setData({
          latitude: res[0].latitude,
          longitude: res[0].longitude,
          localName: res[0].regeocodeData.formatted_address,
          localDesc:res[0].desc,
          markers: [{
            latitude: res[0].latitude,
            longitude: res[0].longitude,
            name: res[0].name,
            desc: res[0].regeocodeData.formatted_address
          }],
        })
        wx.hideLoading()
      }
    })
  },
  //调用高德api获得地理位置
  loadCity: function(latitude, longitude) {
    var that = this;
    var myAmapFun = new amapFile.AMapWX({
      key: markersData.key
    });
    
    myAmapFun.getRegeo({
      location: '' + longitude + ',' + latitude + '', //location的格式为'经度,纬度'
      success: function(data) {
        that.setData({
          localDesc: data[0].desc,
          localName: data[0].regeocodeData.formatted_address
        })
        console.log(data[0].desc);
      },
      fail: function(info) {}
    });

    myAmapFun.getWeather({
      success: function(data) {
        that.setData({
          weather: data
        })
        console.log(data);
        //成功回调
      },
      fail: function(info) {
        //失败回调
        console.log(info)
      }
    })
  },

})