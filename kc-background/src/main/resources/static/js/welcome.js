var vm = new Vue({
    el: '#app',
    data: {
        user:"",
        java_version:"",
        serer_address:"",
        system_info:"",
        envirenment:"",
        time:"",
        timezone:"",
        cpuarch:""
    },
    created:function(){
        this.getSystem()
    },
    mounted: function () {
     this.getSystem()
        setInterval(function(){
           vm.time=vm.timeFormate(new Date())
        },1000)
    },
    methods: {

        show: function () {
            this.visible = true;
        },
        add: function () {

        },
        update: function () {
        },
        del: function () {

        },
       getSystem:function () {
           axios({
               method: 'post',
               url:"../system/info"
           }).then(function(res){
               console.log("tomcat")
             console.log(res.data.env.propertySources[1].source[1].underlyingSource.source.serverInfo)//tomcat
             vm.envirenment=res.data.env.propertySources[1].source[1].underlyingSource.source.serverInfo//运行环境
           console.log("系统")
           console.log(res.data.env.propertySources[1].source[2].underlyingSource.source)//java的详细
           var system=res.data.env.propertySources[1].source[2].underlyingSource.source
           console.log("系统处理")
           console.log(system["java.runtime.version"])
           vm.java_version=system["java.runtime.version"]
           vm.serer_address=system["java.rmi.server.hostname"]
           vm.system_info=system["os.name"]
           vm.timezone=system["user.timezone"]
           vm.cpuarch=system["os.arch"]
           vm.user=res.data.user
              vm.time =vm.timeFormate(new Date())
       })
       },

        timeFormate:function (timeStamp){
            let year = new Date(timeStamp).getFullYear();
            let month =new Date(timeStamp).getMonth() + 1 < 10? "0" + (new Date(timeStamp).getMonth() + 1): new Date(timeStamp).getMonth() + 1;
            let date =new Date(timeStamp).getDate() < 10? "0" + new Date(timeStamp).getDate(): new Date(timeStamp).getDate();
            let hh =new Date(timeStamp).getHours() < 10? "0" + new Date(timeStamp).getHours(): new Date(timeStamp).getHours();
            let mm =new Date(timeStamp).getMinutes() < 10? "0" + new Date(timeStamp).getMinutes(): new Date(timeStamp).getMinutes();
            let ss =new Date(timeStamp).getMinutes() < 10? "0" + new Date(timeStamp).getMinutes(): new Date(timeStamp).getSeconds();
            return year + "年" + month + "月" + date +"日"+" "+hh+":"+mm+":"+ss ;
        }


    }
})
