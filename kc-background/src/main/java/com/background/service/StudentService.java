package com.background.service;

import com.background.model.StudentEntity;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

import java.util.List;

/**
 * author shish
 * Create Time 2019/4/29 11:06
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface StudentService {

    List<StudentEntity> selectList(String class_grade, Integer uid);
    List<StudentEntity> selectListPage(Page<StudentEntity> objectPage, QueryWrapper<StudentEntity> queryWrapper);
    Integer save(StudentEntity studentEntity);
    StudentEntity selectOne(Long userId);
}
