package com.background.mapper;

import com.background.model.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Select;

import java.util.Set;

/**
 * author shish
 * Create Time 2019/3/7 16:47
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface RoleMapper extends BaseMapper<Role> {
    @Select("SELECT r.* from sys_role r LEFT JOIN sys_user_role ur on r.id = ur.role_id where ur.uid  = #{uid}")
    Set<Role> findRolesByUserId(Integer uid);
}
