package com.background.mapper;

import com.background.model.MenuEntity;
import com.background.model.Permission;
import com.background.model.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Set;

/**
 * author shish
 * Create Time 2019/3/7 16:47
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface PermissionMapper extends BaseMapper<MenuEntity> {

    Set<Permission> findPermissionsByRoleId(@Param("roles") Set<Role> roles);
}
