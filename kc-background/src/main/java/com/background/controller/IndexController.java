package com.background.controller;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * author shish
 * Create Time 2019/1/12 14:24
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@Controller
public class IndexController {


    @RequestMapping("/notLogin")
    public String login() {
        return "login";
    }

    @RequestMapping("/")
    public String ind() {
        return "index";
    }

    @RequestMapping("/index")
    public String index() {
        return "index";
    }

    //注册
    @RequestMapping("/register.html")
    public String register() {
        return "register";
    }

    @RequestMapping("/welcome.html")
    public String welcome() {
        return "welcome";
    }


    @RequestMapping("/admin-add.html")
    public String admin_add() {
        return "admin-add";
    }


    @RequestMapping("/admin-cate.html")
    public String admin_cate() {
        return "admin-cate";
    }


    @RequestMapping("/admin-edit.html")
    public String admin_edit() {
        return "admin-edit";
    }

    @RequestMapping("/admin-list.html")
    public String admin_list() {
        return "admin-list";
    }


    @RequestMapping("/admin-role.html")
    public String admin_role() {
        return "admin-role";
    }

    @RequestMapping("/admin-rule.html")
    public String admin_rule() {
        return "admin-rule";
    }

    //打卡管理
    @RequestMapping("/order-list.html")
    public String order_list() {
        return "lesson-list";
    }


}
