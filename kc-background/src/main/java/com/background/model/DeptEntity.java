package com.background.model;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.io.Serializable;

/**
 * author shish
 * Create Time 2019/3/8 10:44
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
@TableName("kc_dept")
public class DeptEntity implements Serializable {

    private static final long serialVersionUID = -5809782578272943999L;
    @TableId(value ="id",type = IdType.AUTO)
    private  Integer id;
    @TableField(value = "dept_name")
    private  String dept_name;
    @TableField(value = "dept_num")
    private  Integer dept_num;
    @TableField(value = "dept_class")
    private  String  dept_class;
    @TableField(value = "dept_grade")
    private  String dept_grade;
    @TableField(value = "uid")
    private  Integer uid;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDept_name() {
        return dept_name;
    }

    public void setDept_name(String dept_name) {
        this.dept_name = dept_name;
    }

    public Integer getDept_num() {
        return dept_num;
    }

    public void setDept_num(Integer dept_num) {
        this.dept_num = dept_num;
    }

    public String getDept_class() {
        return dept_class;
    }

    public void setDept_class(String dept_class) {
        this.dept_class = dept_class;
    }

    public String getDept_grade() {
        return dept_grade;
    }

    public void setDept_grade(String dept_grade) {
        this.dept_grade = dept_grade;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }
}
