package com.background.model;

import com.baomidou.mybatisplus.annotation.TableName;

@TableName("sys_user_role")

public class UserRole {
    private Integer uid;
    private Integer role_id;

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public Integer getRole_id() {
        return role_id;
    }

    public void setRole_id(Integer role_id) {
        this.role_id = role_id;
    }
}
